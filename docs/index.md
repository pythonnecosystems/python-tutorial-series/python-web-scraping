# Python 웹 스크래핑: Beautiful Soup, Requests <sup>[1](#footnote_1)</sup>

<font size="3">Python에서 Beautiful Soup과 Requests 라이브러리를 사용하여 웹 페이지를 스크랩하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./web-scraping.md#intro)
1. [웹 스크래핑이란?](./web-scraping.md#sec_02)
1. [웹 스크래핑에 Python을 사용하는 이유](./web-scraping.md#sec_03)
1. [Beautiful Soup와 Requests 설치와 임포트](./web-scraping.md#sec_04)
1. [간단한 웹 스크래핑 스크립](./web-scraping.md#sec_05)
1. [Beautiful Soup를 사용한 HTML 파싱](./web-scraping.md#sec_06)
1. [웹 페이지에서 데이터 추출](./web-scraping.md#sec_07)
1. [오류와 예외 처리](./web-scraping.md#sec_08)
1. [스크랩된 데이터 저장과 내보내기](./web-scraping.md#sec_09)
1. [요약](./web-scraping.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 37 — Python Web Scraping: Beautiful Soup, Requests](https://levelup.gitconnected.com/python-tutorial-37-python-web-scraping-beautiful-soup-requests-429d4f2557e3?sk=676b160342720805d1f0ac8506a9879d)를 편역하였습니다.

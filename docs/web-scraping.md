# Python 웹 스크래핑: Beautiful Soup, Requests

## <a name="intro"></a> 개요
웹 스크래핑은 웹 페이지에서 데이터를 추출하여 자신의 분석에 사용할 수 있는 형식으로 저장할 수 있는 기술이다. 웹 스크래핑은 다음과 같은 다양한 목적에 유용할 수 있다.

- 서로 다른 출처의 정보를 수집하여 한 곳에 모아 집계한다.
- API 또는 다운로드 가능한 파일을 통해 사용할 수 없는 데이터를 추출한다.
- 웹 페이지와 상호 작용하는 작업을 자동화한다.
- 시간에 따른 웹 콘텐츠의 변화를 모니터링한다.

이 포스팅에서는 Python에서 Beautiful Soup과 Requests를 사용하여 웹 페이지를 스크래핑하는 방법을 설명한다. Beautiful Soup는 HTML과 XML 문서를 파싱하고 그로부터 데이터를 추출할 수 있는 라이브러리이다. Requests는 HTTP 요청을 보내고 응답을 처리할 수 있는 라이브러리이다. 이 라이브러리를 함께 사용하면 웹 페이지에 프로그래밍 방식으로 접근하고 조작할 수 있다.

이 포스팅을 일고 이해한다면, 다음 작업을 수행할 수 있다.

1. Beautiful Soup와 Requests를 설치하고 임포트한다.
1. 웹 페이지를 가져와 콘텐츠를 인쇄하는 간단한 웹 스크래핑 스크립트를 작성한다.
1. Beautiful Soup로 HTML을 해석하고 웹 페이지의 다양한 요소를 액세스한다.
1. 웹 페이지에서 데이터를 추출하여 구조화된 형식으로 저장한다.
1. 웹 스크래핑 중 발생할 수 있는 오류와 예외를 처리한다.
1. 스크랩된 데이터를 파일 또는 데이터베이스에 저장하고 내보낸다.

시작하기 전에 Python과 데이터 분석에 대한 기본적인 이해가 필요하다. 또한 컴퓨터에 Python 3과 코드 에디터가 설치되어 있어야 한다. Python을 처음 접하거나 복습이 필요한 경우 [Python 튜토리얼]()을 참고하세요.

스크래핑할 준비 됐어나요, 시작하자!

## <a name="sec_02"></a> 웹 스크래핑이란?
웹 스크래핑(web scraping)은 웹 페이지에서 데이터를 추출하여 분석 또는 다른 목적으로 사용할 수 있는 형식으로 저장하는 과정이다. 웹 스크래핑은 수동으로, 웹 페이지로부터 데이터를 복사하여 붙여넣기를 통해, 또는 웹 페이지에 접근하여 조작할 수 있는 프로그램 또는 도구를 사용하여 자동으로 수행될 수 있다.

웹 스크래핑은 일반적으로 페치(fetching)와 파싱(parsing)의 두 단계를 포함한다. 페치는 웹 서버에 요청을 보내고 웹 페이지의 HTML 코드를 포함하는 응답을 받는 과정이다. 파싱은 HTML 코드를 분석하여 관심 있는 데이터를 추출하는 과정이다. 예를 들어, 온라인 서점에서 책의 제목, 저자, 가격을 긁어내고 싶다면 책 정보가 포함된 웹 페이지를 가져와 HTML 코드를 파싱하여 제목, 저자, 가격에 해당하는 요소를 찾아야 한다.

웹 스크래핑은 웹페이지의 복잡성과 구조, 추출하고자 하는 데이터에 따라 다양한 방식으로 이루어질 수 있다. 웹 스크래핑을 위한 일반적인 방법과 도구들은 다음과 같다.

- **웹 브라우저**: Chrome, Firefox 또는 Edge와 같은 웹 브라우저는 웹 페이지의 소스 코드를 읽고 HTML 요소를 검사할 수 있다. 또한 개발자 도구나 확장 프로그램을 사용하여 웹 페이지에서 데이터를 복사하거나 내보낼 수도 있다.
- **웹 스크래핑 소프트웨어**: Scrapy, Octoparse 또는 ParseHub와 같은 웹 스크래핑 소프트웨어는 코드를 작성하지 않고 웹 스크래핑 프로젝트를 생성하고 실행할 수 있게 해주는 어플리케이션이다. 이들은 일반적으로 스크래핑할 데이터를 선택하고 스크래핑 프로세스에 대한 설정과 옵션을 구성할 수 있는 그래픽 사용자 인터페이스(GUI)를 제공한다.
- **웹 스크래핑 라이브러리**: Beautiful Soup, Requests 또는 셀레늄과 같은 웹 스크래핑 라이브러리는 Python, 자바, 루비와 같은 프로그래밍 언어로 가져와 사용할 수 있는 모듈 또는 패키지이다. 이들은 웹 페이지를 읽어 파싱하고 데이터를 프로그램적으로 추출할 수 있는 기능과 방법을 제공한다.

이 포스팅에서는 Python에서 `Beautiful Soup`와 `Requests` 라이브러리를 사용하여 웹 페이지를 스크랩한다. 이러한 라이브러리는 다양한 웹 스크랩 작업을 처리할 수 있는 강력한 도구이다. 설치하고 가져오는 방법, 간단한 웹 스크랩 스크립트를 만드는 방법, Beautiful Soup로 HTML을 파싱하는 방법, 웹 페이지에서 데이터를 추출하는 방법, 오류와 예외 처리 방법, 스크랩된 데이터를 저장하고 내보내는 방법 등을 설명한다.

## <a name="sec_03"></a> 웹 스크래핑에 Python을 사용하는 이유
Python은 웹 스크래핑에 널리 사용되는 프로그래밍 언어 중 하나이다. Python이 웹 스크래핑에 훌륭한 선택이 되는 이유는 다음과 같다.

- **배우고 사용하기 쉽다**: Python은 코드를 읽고 쓰기 쉽게 하는 간단하고 표현적인 구문을 가지고 있다. Python은 또한 초보자와 전문가 모두를 위한 많은 리소스와 튜토리얼을 제공하는 크고 활동적인 커뮤니티를 가지고 있다.
- **풍부하고 다양한 라이브러리**: Python은 웹 페이지 가져오기와 구문 분석, 데이터 추출과 처리, 오류와 예외 처리, 데이터 저장과 내보내기 등과 같은 다양한 웹 스크래핑 작업을 도울 수 있는 거대한 라이브러리 모음을 가지고 있다. Python에서 가장 인기 있고 강력한 웹 스크래핑 라이브러리로는 Beautiful Soup, Requests, Selenium, Scrapy 및 LXML이 있다.
- **유연하고 다용도**: Python은 HTML, XML, JSON, CSV, 이미지, 비디오 등 다양한 유형의 웹 페이지와 데이터를 처리할 수 있다. Python은 또한 웹 브라우저, 웹 스크래핑 소프트웨어, 웹 스크래핑 라이브러리 등과 같은 다양한 웹 스크래핑 방법과 도구를 함께 작동할 수 있다. Python은 Javascript, Django, Flask 등 다른 언어와 프레임워크에도 통합할 수 있다.

이 포스팅에서는 Python에서 `Beautiful Soup`와 `Requests` 라이브러리를 사용하여 웹 페이지를 스크랩한다. 이러한 라이브러리는 설치와 임포트하기 쉽고, 웹 페이지에 접근하고 프로그래밍 방식으로 조작할 수 있는 간단하고 편리한 방법을 제공한다. 이러한 라이브러리를 사용하여 웹 페이지를 가져와(fetch) 파싱하고, 웹 페이지에서 데이터를 추출하고, 오류와 예외를 처리하고, 스크랩된 데이터를 저장하고 내보내는(export) 방법을 보일 것이다.

## <a name="sec_04"></a> Beautiful Soup와 Requests 설치와 임포트
`Beautiful Soup`와 `Requests` 라이브러리를 사용하기 전에 먼저 설치하고 임포트해야 한다. 라이브러리를 설치한다는 것은 라이브러리를 사용하는 데 필요한 파일과 종속성을 다운로드하여 설치하는 것을 의미한다. 라이브러리를 임포트한다는 것은 라이브러리의 함수와 메서드를 사용할 수 있도록 Python 프로그램에 라이브러리를 로드하는 것을 의미한다.

`Beautiful Soup`와 `Requests` 라이브러리를 사용하기 위해서는 Python 패키지를 설치하고 관리할 수 있는 도구인 `pip` 명령을 사용한다. pip은 보통 Python 설치에 포함되어 있지만, pip이 없다면 [이 페이지](https://pip.pypa.io/en/stable/installation/)에 따라 설치하면 된다.

`pip`을 사용하여 `Beautiful Soup`와 `Requests` 라이브러리를 설치하려면 터미널 또는 명령 프롬프트를 열고 다음 명령을 입력해야 한다.

```bash
# Install the Beautiful Soup library
pip install beautifulsoup4
# Install the requests library
pip install requests
```

이 명령어는 Beautiful Soup의 최신 버전을 다운로드하여 설치하고 라이브러리와 그 종속성을 요청한다. `pip install beautiful soup4==4.9.3`와 같이 라이브러리 이름 뒤에 `==version_number`를 추가하여 특정 버전의 라이브러리를 지정할 수도 있다.

라이브러리를 설치했으면 `import` 문을 사용하여 Python 프로그램으로 임포트할 수 있다. `import` 문을 사용하면 라이브러리 이름이나 별칭을 사용하여 라이브러리의 함수와 메서드를 액세스할 수 있다. 예를 들어 다음과 같이 Beautiful Soup와 `Requests` 라이브러리를 임포트할 수 있다.

```python
# Import the Beautiful Soup library
from bs4 import BeautifulSoup
# Import the requests library
import requests
```

여기서 `BeautifulSoup` 클래스는 HTML을 Beautiful Soup으로 구문 분석하는 데 사용할 메인 클래스인 `bs4` 모듈에서 임포트한다. 웹 페이지를 가져오는 데 사용할 함수를 포함하는 `request` 모듈도 임포트한다. 또한 코드에서 `requests` 대신 `r`을 사용할 수 있도록 `import requests as r` 같이 라이브러리 이름에 별칭을 사용할 수도 있다.

Beautiful Soup와 Requests 라이브러리를 설치하고 임포트하고 첫 번째 웹 스크래핑 스크립트를 만들 준비가 되었다. 다음 절에서는 이러한 라이브러리를 사용하여 웹 페이지를 가져와 콘텐츠를 출력하는 방법에 대해 설명한다.

## <a name="sec_05"></a> 간단한 웹 스크래핑 스크립
이 절에서는 웹 페이지를 가져와 Beautiful Soup와 Requests 라이브러를 사용하여 콘텐츠를 출력하는 간단한 웹 스크래핑 스크립트를 만드는 방법을 보인다. 이 스크립트는 자신의 웹 스크래핑 프로젝트를 위해 수정하고 확장할 수 있는 기본 템플릿 역할을 할 것이다.

간단한 웹 스크래핑 스크립트를 만들려면 다음 단계를 따라야 한다.

&nbsp; 1. 새 Python 파일을 만들고 `web_scraping.py` 파일에 지정한다.

&nbsp; 2. 파일 상단에 다음 행을 추가하여 Beautiful Soup과 requests를 임포트한다.

```python
# Import the Beautiful Soup library
from bs4 import BeautifulSoup
# Import the requests library
import requests
```
&nbsp; 3. 스크랩할 웹 페이지의 URL을 포함하는 변수를 정의한다. 예를 들어, 웹 스크랩에 관한 위키피디아 페이지의 URL: `https://en.wikipedia.org/wiki/Web_scraping`을 사용할 수 있다. URL을 다음과 같이 `url`이라는 이름의 변수에 할당한다.

```python
# Define the URL of the web page
url = "https://en.wikipedia.org/wiki/Web_scraping"
```

&nbsp; 4. `requests.get()` 함수를 사용하여 웹 서버에 GET 요청을 보내고 웹 페이지를 가져온다. `requests.get()` 함수는 URL을 인수로 받아 웹 페이지의 상태 코드, 헤더 및 내용을 포함한 `Response` 객체를 반환한다. `Response` 객체를 다음과 같이 `response`라는 이름의 변수에 할당한다.

```python
# Fetch the web page
response = requests.get(url)
```

&nbsp; 5. 응답의 상태 코드를 확인하여 요청이 성공했는지 체크한다. 상태 코드는 요청의 결과를 나타내는 정수이다. 상태 코드 `200`은 요청이 성공했음을 의미하고, 상태 코드 `404`는 웹 페이지를 찾을 수 없음을 의미한다. `response.status_code` 속성을 사용하여 응답의 상태 코드에 액세스할 수 있다. `if-else` 문을 사용하여 다음과 같이 상태 코드를 기반으로 메시지를 출력할 수 있다.

```python
# Check the status code
if response.status_code == 200:
    print("Request successful")
else:
    print("Request failed")
```

&nbsp; 6. `response.content` 속성을 사용하여 웹 페이지의 콘텐츠에 접근한다. `response.content` 속성은 웹 페이지의 HTML 코드를 바이트 객체로 반환한다. `decode()` 메서드를 사용하고 웹 페이지의 인코딩을 지정하여 바이트 객체를 문자열로 변환할 수 있다. 예를 들어, 다음과 같이 UTF-8 인코딩을 사용할 수 있다.

```python
# Access the content of the web page
html = response.content.decode("utf-8")
```

&nbsp; 7. `print()` 함수를 사용하여 웹 페이지의 내용을 출력한다. `len()` 함수를 사용하여 내용의 길이를 다음과 같이 출력할 수도 있다.

```python
# Print the content of the web page
print(html)
# Print the length of the content
print(len(html))
```

&nbsp; 8. `web_scraping.py` 파일에 저장하고 실행한다. 요청 상태, 내용, 웹 페이지의 길이를 보여주는 출력을 봐야 한다. 예를 들어, 웹 스크래핑에 관한 위키피디아 페이지의 출력은 다음과 같이 보인다.

```
Request successful

...
114512
```

축하합니다! 방금 Beautiful Soup와 requests 라이브러리를 사용하여 Python으로 첫 웹 스크래핑 스크립트를 만들었다. 이 라이브러리를 사용하여 웹 페이지의 콘텐츠를 가져오고 출력하는 방법을 보였다. 다음 절에서는 Beautiful Soup 라이브러리를 사용하여 HTML 코드를 파싱하고 웹 페이지의 여러 요소를 액세스하는 방법을 설명한다.

## <a name="sec_06"></a> Beautiful Soup를 사용한 HTML 파싱
앞 절에서 requests 라이브러리를 사용하여 웹 페이지의 콘텐츠를 가져오고 출력하는 방법을 보였다. 그러나 웹 페이지의 콘텐츠는 보통 데이터를 읽거나 추출하기가 쉽지 않은 크고 복잡한 HTML 코드이다. 웹 스크래핑 과정을 더 쉽고 효율적으로 하려면 HTML 코드를 파싱하여 제목, 표제어(headings), 단락, 링크, 이미지 등 웹 페이지의 다양한 요소를 액세스해야 한다. 이것이 Beautiful Soup 라이브러리가 유용한 부분이다.

Beautiful Soup은 HTML과 XML 문서를 파싱하고 그로부터 데이터를 추출할 수 있는 라이브러리이다. Beautiful Soup은 잘 형성된 코드, 잘못 형성된 코드 또는 불완전한 코드와 같은 다양한 유형의 HTML 코드를 처리할 수 있다. Beautiful Soup은 HTML 코드를 방문과 조작할 수 있는 트리 구조로 변환할 수 있는 모듈인 `html.parser`, `lxml` 또는 `html5lib` 같은 다른 파서와도 작동할 수 있다.

Beautiful Soup로 HTML을 파싱하려면 다음 단계를 따라야 한다.

&nbsp; 1. `web_scrapping.py` 파일 상단에 다음 행을 추가하여 Beautiful Soup 라이브러리를 임포트한다.

```python
# Import the Beautiful Soup library
from bs4 import BeautifulSoup
```

&nbsp; 2. request 라이브러리를 사용하여 가져온 웹 페이지의 HTML 코드를 포함하는 `BeautifulSoup` 객체를 생성한다. `BeautifulSoup` 객체를 생성하려면 인수로 사용할 HTML 코드와 파서의 이름을 `BeautifulSoup` 클래스에 전달해야 한다. 예를 들어 다음과 같이 `html.parser` 파서를 사용할 수 있다.

```python
# Create a BeautifulSoup object
soup = BeautifulSoup(html, "html.parser")
```

&nbsp; 3. `BeautifulSoup` 객체의 메서드와 속성을 사용하여 웹 페이지의 다양한 요소를 액세스할 수 있다. 예를 들어, 다음과 같은 메서드와 속성을 사용하여 웹 페이지의 일부 일반적인 요소를 액세스할 수 있다.

- **soup.title**: 웹 페이지의 제목 요소를 반환한다.
- **soup.title.string**: 웹 페이지의 제목 요소의 텍스트를 반환한다.
- **soup.head**: 웹 페이지의 헤드 요소를 반환한다.
- **soup.body**: 웹 페이지의 본문 요소를 반환한다.
- **soup.find(tag)**: 태그 이름과 일치하는 웹 페이지의 첫 번째 요소를 반환한다.
- **soup.find_all(tag)**: 태그 이름과 일치하는 웹 페이지의 모든 요소 리스트를 반환한다.
- **soup.find(id)**: 지정된 ID 특성을 가진 웹 페이지의 요소를 반환한다.
- **soup.find(class_)**: 지정된 클래스 특성을 가진 웹 페이지의 요소를 반환한다.

&nbsp; 4. `print()` 함수를 사용하여 액세스한 요소를 출력한다. `type()` 함수를 사용하여 액세스한 요소의 타입을 인쇄할 수도 있다. 예를 들어, 다음과 같이 제목 요소와 그 타입을 인쇄할 수 있다.

```python
# Print the title element and its type
print(soup.title)
print(type(soup.title))
```

&nbsp; 5. `web_scraping.py` 파일을 저장하고 실행한다. 접근한 요소들과 그것들의 타입을 보여주는 출력한다. 예를 들어, 웹 스크래핑에 관한 위키피디아 페이지의 출력은 다음과 같이 보인다.

```
Request successful
```

방금 Beautiful Soup으로 HTML을 파싱하고 웹 페이지의 일부 요소를 액세스했다. `BeautifulSoup` 객체를 만들고 그 메서드와 속성을 사용하여 웹 페이지의 여러 요소를 액세스하는 방법을 보였다. 다음 절에서는 Beautiful Soup 라이브러리를 사용하여 웹 페이지에서 데이터를 추출하고 구조화된 형식으로 저장하는 방법을 설명한다.

## <a name="sec_07"></a> 웹 페이지에서 데이터 추출
Beautiful Soup으로 HTML을 파싱하고 웹 페이지의 여러 요소에 접근하는 방법을 배웠으므로 웹 페이지에서 데이터 추출을 시작하여 구조화된 형식으로 저장할 수 있다. 웹 페이지에서 데이터를 추출한다는 것은 텍스트, 이미지, 링크, 테이블 등 관심 있는 정보를 찾아 검색하는 것을 의미한다. 구조화된 형식으로 데이터를 저장한다는 것은 목록, 사전, CSV 파일, 데이터베이스 등 접근과 분석이 용이한 방식으로 데이터를 구성하고 저장하는 것을 의미한다.

웹 페이지에서 데이터를 추출하여 구조화된 형식으로 저장하려면 다음 단계를 수행해야 한다.

1. 웹 페이지에서 추출하고자 하는 데이터를 파악한다. 예를 들어, 온라인 서점에서 책의 제목, 저자, 가격 등을 추출하려면 웹 페이지에서 이러한 정보를 포함하고 있는 요소를 찾아야 한다.
1. `BeautifulSoup` 객체의 메서드와 속성을 사용하여 추출하려는 데이터를 포함하는 요소를 찾고 액세스할 수 있다. 예를 들어 `soup.find()` 또는 `soup.find_all()` 메서드를 사용하여 태그 이름, id, 클래스 또는 기타 속성으로 요소를 찾을 수 있다. 또한 `element.text` 또는 `element.get()` 속성을 사용하여 텍스트 또는 요소 값을 가져올 수 있다.
1. 웹 페이지에서 추출한 데이터를 저장할 수 있는 데이터 구조를 만든다. 예를 들어, 리스트, 사전 또는 pandas DataFrame을 사용하여 데이터를 표 형식으로 저장할 수 있다. 커스텀 클래스 또는 객체를 사용하여 데이터를 객체 지향 형식으로 저장할 수도 있다.
1. 웹 페이지에서 추출한 데이터를 생성한 데이터 구조에 추가하거나 할당한다. 예를 들어, `append()` 또는 `extend()` 메서드를 사용하여 데이터를 리스트에 추가하거나, `update()` 또는 `setdefault()` 메서드를 사용하여 데이터를 사전에 추가하거나, `loc()` 또는 `iloc()` 메서드를 사용하여 pandas DataFrame에 데이터를 추가할 수 있다.
1. 웹 페이지에서 추출한 데이터를 포함하는 데이터 구조를 출력하거나 저장한다. 예를 들어, `print()` 함수를 사용하여 데이터 구조를 콘솔에 인쇄하거나 `open()`과 `write()` 함수를 사용하여 데이터 구조를 텍스트 또는 CSV 파일에 저장하거나 `to_sql()` 또는 `to_csv()` 메서드를 사용하여 데이터 구조를 데이터베이스 또는 CSV 파일에 저장할 수 있다.

이러한 단계를 설명하기 위해 제목, 작가, 장르, 가격 등 책에 대한 몇 가지 정보가 포함된 예시 웹 페이지를 사용해 보자. 웹 페이지는 https://books.toscrape.com/ 라는 URL을 통해 액세스할 수 있다. 이 웹 페이지는 가상의 온라인 서점으로 웹 스크래핑 연습을 위해 설계되었다. Beautiful Soup와 requests 라이브러리를 사용하여 이 웹 페이지에서 데이터를 추출하고 구조화된 형식으로 저장할 수 있다.

다음은 웹 페이지에서 데이터를 추출하여 사전 리스트에 저장하는 웹 스크래핑 스크립트의 예이다.

```python
# Import the Beautiful Soup and requests libraries
from bs4 import BeautifulSoup
import requests
# Define the URL of the web page
url = "https://books.toscrape.com/"
# Fetch the web page
response = requests.get(url)
# Check the status code
if response.status_code == 200:
    print("Request successful")
else:
    print("Request failed")
# Access the content of the web page
html = response.content.decode("utf-8")
# Create a BeautifulSoup object
soup = BeautifulSoup(html, "html.parser")
# Create an empty list to store the data
data = []
# Find all the article elements that contain the book information
articles = soup.find_all("article", class_="product_pod")
# Loop through each article element
for article in articles:
    # Create an empty dictionary to store the data for each book
    book = {}
    # Find the h3 element that contains the title of the book
    h3 = article.find("h3")
    # Find the a element that contains the link and the title of the book
    a = h3.find("a")
    # Get the title of the book from the title attribute of the a element
    title = a.get("title")
    # Get the link of the book from the href attribute of the a element
    link = a.get("href")
    # Find the p element that contains the genre of the book
    p = article.find("p", class_="genre")
    # Get the genre of the book from the text of the p element
    genre = p.text
    # Find the p element that contains the price of the book
    p = article.find("p", class_="price_color")
    # Get the price of the book from the text of the p element
    price = p.text
    # Find the p element that contains the author of the book
    p = article.find("p", class_="author")
    # Get the author of the book from the text of the p element
    author = p.text
    # Add the title, link, genre, price, and author to the book dictionary
    book["title"] = title
    book["link"] = link
    book["genre"] = genre
    book["price"] = price
    book["author"] = author
    # Append the book dictionary to the data list
    data.append(book)
# Print the data list
print(data)
# Print the length of the data list
print(len(data))
```

이 `web_scraping.py` 파일을 저장하고 실행하면 데이터 리스트와 그 길이를 나타내는 출력이 디스플레이된다. 데이터 리스트에는 웹 페이지의 책을 나타내는 20개의 사전이 포함되어 있다. 각 사전에는 제목, 링크, 장르, 가격 및 저자의 다섯 가지 키가 있다. 예를 들어 웹 페이지의 출력은 다음과 같이 보인다.

```
Request successful
[{'title': 'A Light in the Attic', 'link': 'catalogue/a-light-in-the-attic_1000/index.html', 'genre': 'Poetry', 'price': '£51.77', 'author': 'Shel Silverstein'}, {'title': 'Tipping the Velvet', 'link': 'catalogue/tipping-the-velvet_999/index.html', 'genre': 'Historical Fiction', 'price': '£53.74', 'author': 'Sarah Waters'}, ... , {'title': 'The Requiem Red', 'link': 'catalogue/the-requiem-red_1/index.html', 'genre': 'Young Adult', 'price': '£22.65', 'author': 'Brynn Chapman'}]
20
```

Beautiful Soup 라이브러리를 사용하여 웹 페이지에서 데이터를 추출하고 구조화된 형식으로 저장했다. `BeautifulSoup` 객체의 메서드과 속성을 사용하여 웹 페이지에서 관심 있는 데이터를 식별하고 찾고 액세스하고 저장하는 방법을 보였다. 다음 절에서는 rewuests와 Beautiful Soup 라이브러리를 사용하여 웹 스크래핑 중 발생할 수 있는 오류와 예외를 처리하는 방법을 설명한다.

## <a name="sec_08"></a> 오류와 예외 처리
웹 스크래핑은 웹 서버와 웹 페이지와의 상호 작용을 수반하는 프로세스로, 항상 예상대로 작동하지 않을 수 있다. 때때로 웹 스크래핑 스크립트를 중단하거나 종료할 수 있는 오류와 예외에 직면할 수 있다. 오류와 예외는 프로그램 또는 웹 서버에서 무엇인가가 잘못되었음을 나타내는 이벤트이다. 예를 들어, 다음과 같은 오류와 예외에 직면할 수 있다.

- **연결 오류(ConnectionError)**: requests 라이브러리가 웹 서버와의 연결을 설정하지 못할 때 발생하는 예외이다. 네트워크 문제, 방화벽 설정 또는 서버 다운타임으로 인해 발생할 수 있다.
- **HTTP 오류(HTTPError)**: requests 라이브러리가 404(Not Found) 또는 500(Internal Server Error) 같은 오류 상태 코드를 가진 HTTP 응답을 수신할 때 발생하는 예외이다. 이는 잘못된 URL, 서버 오류 또는 액세스 제한으로 인해 발생할 수 있다.
- **Timeout**: requests 라이브러리가 지정된 시간 제한 내에 웹 서버로부터 응답을 받지 못할 때 발생하는 예외이다. 이는 느린 네트워크, 많은 트래픽 또는 서버 과부하로 인해 발생할 수 있다.
- **AttributeError**: 존재하지 않는 객체의 속성 또는 메서드를 액세스하려고 할 때 발생하는 오류이다. 오타, 잘못된 구문 또는 누락된 요소로 인해 발생할 수 있다.
- **IndexError**: 리스트의 요소 또는 범위를 벗어난 문자열에 접근하려고 할 때 발생하는 오류이다. 잘못된 인덱스, 빈 리스트 또는 누락된 요소로 인해 발생할 수 있다.
- **KeyError**: 존재하지 않는 사전의 값에 접근하려고 할 때 발생하는 오류이다. 잘못된 키, 빈 사전 또는 누락된 요소로 인해 발생할 수 있다.

오류와 예외를 처리하려면 코드 블록을 실행하고 발생할 수 있는 오류나 예외를 잡을 수 있는 제어 구조인 try-except 문을 사용해야 한다. try-except 문은 다음과 같은 구문을 가진다.

```python
try:
    # Block of code that may raise errors or exceptions
except Exception as e:
    # Block of code that handles the errors or exceptions
```

try 블록에는 웹 페이지 가져오기와 구문 분석, 데이터 추출과 저장 등 실행하고자 하는 코드가 포함되어 있다. exception 블록에는 오류 메시지 인쇄, 오류 기록, request 재시도 등 오류 또는 예외가 발생했을 때 실행하고자 하는 코드가 포함되어 있다. `Exception`은 Python에서 모든 오류와 예외의 기본 클래스를 나타내는 클래스이다. `e`는 오류 또는 예외에 대한 정보를 포함하는 오류 또는 예외 객체의 종류, 메시지, traceback 등을 저장하는 변수이다.

여러 개의 예외 블록을 사용하여 다른 유형의 오류 또는 예외를 개별적으로 처리할 수도 있다. 예를 들어 다음 구문을 사용하여 ConnectionError, HTTPEerror 및 Timeout 예외를 다르게 처리할 수 있다.

```python
try:
    # Block of code that may raise errors or exceptions
except ConnectionError as e:
    # Block of code that handles the ConnectionError
except HTTPError as e:
    # Block of code that handles the HTTPError
except Timeout as e:
    # Block of code that handles the Timeout
```

오류와 예외를 처리하는 방법을 설명하기 위해 앞 절에서 만든 웹 스크래핑 스크립트를 수정해 보자. 웹 페이지를 가져오고 파싱할 때 발생할 수 있는 오류와 예외를 처리하기 위해 try-exception 문을 추가할 것이다. 또한 발생하는 오류 또는 예외의 타입과 메시지를 인쇄하기 위해 `print()` 함수를 추가한다. 수정된 웹 스크래핑 스크립트는 다음과 같다.

```python
# Import the Beautiful Soup and requests libraries
from bs4 import BeautifulSoup
import requests
# Define the URL of the web page
url = "https://books.toscrape.com/"
# Use a try-except statement to handle errors and exceptions
try:
    # Fetch the web page
    response = requests.get(url)
    # Check the status code
    if response.status_code == 200:
        print("Request successful")
    else:
        print("Request failed")
    # Access the content of the web page
    html = response.content.decode("utf-8")
    # Create a BeautifulSoup object
    soup = BeautifulSoup(html, "html.parser")
    # Create an empty list to store the data
    data = []
    # Find all the article elements that contain the book information
    articles = soup.find_all("article", class_="product_pod")
    # Loop through each article element
    for article in articles:
        # Create an empty dictionary to store the data for each book
        book = {}
        # Find the h3 element that contains the title of the book
        h3 = article.find("h3")
        # Find the a element that contains the link and the title of the book
        a = h3.find("a")
        # Get the title of the book from the title attribute of the a element
        title = a.get("title")
        # Get the link of the book from the href attribute of the a element
        link = a.get("href")
        # Find the p element that contains the genre of the book
        p = article.find("p", class_="genre")
        # Get the genre of the book from the text of the p element
        genre = p.text
        # Find the p element that contains the price of the book
        p = article.find("p", class_="price_color")
        # Get the price of the book from the text of the p element
        price = p.text
        # Find the p element that contains the author of the book
        p = article.find("p", class_="author")
        # Get the author of the book from the text of the p element
        author = p.text
        # Add the title, link, genre, price, and author to the book dictionary
        book["title"] = title
        book["link"] = link
        book["genre"] = genre
        book["price"] = price
        book["author"] = author
        # Append the book dictionary to the data list
        data.append(book)
    # Print the data list
    print(data)
    # Print the length of the data list
    print(len(data))
# Handle the errors and exceptions
except Exception as e:
    # Print the type and the message of the error or exception
    print(type(e))
    print(e)
```

이 `web_scraping.py` 파일을 저장하고 실행하면 오류나 예외가 발생하지 않으면 이전과 동일한 출력이 표시되어야 한다. 그러나 오류나 예외가 발생하면 오류나 예외의 타입과 메시지를 보여주어야 한다. 예를 들어 "https://books.toscrape.com/invalid" 같이 URL을 잘못된 것으로 변경하면 다음과 같이 `HTTPError` 예외가 표시되는 출력이 디스플레이된다.

```
Request failed

404 Client Error: Not Found for url: https://books.toscrape.com/invalid
```

방금 request와 Beautiful Soup 라이브러리를 사용하여 웹 스크래핑 중 발생할 수 있는 오류와 예외를 처리했다. 코드 블록을 실행하고 발생할 수 있는 오류 또는 예외를 잡기 위해 try-exception 문을 사용하는 방법을 보였다. 또한 발생하는 오류 또는 예외의 타입과 메시지를 출력하는 방법도 보였다. 다음 절에서는 Python 표준 라이브러리와 pandas 라이브러리를 사용하여 스크래핑된 데이터를 파일 또는 데이터베이스로 저장하고 내보내는 방법도 설명한다.

## <a name="sec_09"></a> 스크랩된 데이터 저장과 내보내기
앞 절에서는 Beautiful Soup 라이브러리를 이용하여 웹 페이지에서 데이터를 추출하여 구조화된 형식으로 저장하는 방법을 보였다. 그러나 데이터를 추가 분석이나 시각화에 사용하려면 목록이나 사전과 같은 데이터 구조에 저장하는 것만으로는 충분하지 않다. 데이터를 저장하고 다른 도구나 애플리케이션에 접근하고 조작할 수 있는 파일이나 데이터베이스로 내보내야 한다.

스크래핑된 데이터를 파일이나 데이터베이스로 저장하고 내보내기 위해서는 파일과 데이터베이스 작업을 위한 기능과 방법을 제공하는 모듈인 Python 표준 라이브러리나 pandas 라이브러리를 이용해야 한다. Python 표준 라이브러리에는 `os`, `sys`, `csv`, `json`, `sqlite3` 등의 모듈이 포함되어 있다. pandas 라이브러리는 Python의 고성능 데이터 구조와 데이터 분석 도구를 제공하는 서드파티 라이브러리이다.

스크랩된 데이터를 저장하고 파일 또는 데이터베이스로 내보내려면 다음 단계를 수행해야 한다.

&nbsp; 1. `web_scrapping.py` 파일 상단에 다음 행을 추가하여 Python 표준 라이브러리 또는 pandas 라이브러리를 임포트한다.

```python
# Import the Python standard library
import os
import sys
import csv
import json
import sqlite3
# Import the pandas library
import pandas as pd
```

&nbsp; 2. 데이터를 저장하고 내보낼 형식과 대상을 선택한다. 예를 들어, 데이터를 텍스트 파일, CSV 파일, JSON 파일 또는 SQLite 데이터베이스에 저장하고 내보낼 수 있다. 또한 데이터를 저장하여 컴퓨터, 클라우드 서비스 또는 웹 서버와 같은 로컬 또는 원격 위치로 내보낼 수 있다.

&nbsp; 3. Python 표준 라이브러리나 pandas 라이브러리의 함수과 메서드를 사용하여 데이터를 선택한 형식과 대상에 저장하고 내보낼 수 있다. 예를 들어 다음과 같은 함수과 메서드를 사용하여 다른 형식과 대상에 데이터를 저장하고 내보낼 수 있다.

- `open()`와 `write()`: 파일에 데이터를 열고 쓸 수 있는 Python 표준 라이브러리의 함수이다. 이 함수를 사용하여 데이터를 저장하고 텍스트 파일이나 CSV 파일로 내보낼 수 있다. 예를 들어, 다음 코드를 사용하여 데이터를 저장하고 books.csv라는 이름의 CSV 파일로 내보낼 수 있다.

```python
# Open a CSV file named books.csv
with open("books.csv", "w") as f:
    # Create a CSV writer object
    writer = csv.writer(f)
    # Write the header row
    writer.writerow(["title", "link", "genre", "price", "author"])
    # Loop through the data list
    for book in data:
        # Write each book as a row
        writer.writerow([book["title"], book["link"], book["genre"], book["price"], book["author"]])
```

- **json.dump()**: 데이터를 JSON 파일로 직렬화하여 쓸 수 있도록 해주는 Python 표준 라이브러리의 함수이다. 이 함수를 사용하여 데이터를 저장하고 JSON 파일로 내보낼 수 있다. 예를 들어, 다음 코드를 사용하여 `books.json`이라는 이름의 JSON 파일로 데이터를 저장하고 내보낼 수 있다.

```python
# Open a JSON file named books.json
with open("books.json", "w") as f:
    # Write the data list as a JSON object
    json.dump(data, f)
```

- `sqlite3.connect()`와 `curser.execute()`: SQLite 데이터베이스에 데이터를 연결하고 쓸 수 있도록 해주는 Python 표준 라이브러리의 함수와 메소드들이다. 이러한 함수와 메소드들을 사용하여 데이터를 저장하여 SQLite 데이터베이스로 내보낼 수 있다. 예를 들어, 다음 코드를 사용하여 `books.db`라는 이름의 SQLite 데이터베이스로 데이터를 저장하고 내보낼 수 있다.

```python
# Connect to a SQLite database named books.db
conn = sqlite3.connect("books.db")
# Create a cursor object
cursor = conn.cursor()
# Create a table named books
cursor.execute("CREATE TABLE IF NOT EXISTS books (title TEXT, link TEXT, genre TEXT, price TEXT, author TEXT)")
# Loop through the data list
for book in data:
    # Insert each book as a row
    cursor.execute("INSERT INTO books VALUES (?, ?, ?, ?, ?)", (book["title"], book["link"], book["genre"], book["price"], book["author"]))
# Commit the changes
conn.commit()
# Close the connection
conn.close()
```

- `pd.DataFrame()`과 `to_csv()` 또는 `to_sql()`: `DataFrame` 객체를 생성하고 CSV 파일 또는 SQLite 데이터베이스에 데이터를 쓸 수 있는 pandas 라이브러리의 메서드이다. 이 메서드를 사용하여 CSV 파일 또는 SQLite 데이터베이스에 데이터를 저장하고 내보낼 수 있다. 예를 들어 다음 코드를 사용하여 `books.csv`라는 이름의 CSV 파일 또는 `books.db`라는 이름의 SQLite 데이터베이스에 데이터를 저장하고 내보낼 수 있다.

```python
# Create a DataFrame object from the data list
df = pd.DataFrame(data)
# Save and export the DataFrame to a CSV file named books.csv
df.to_csv("books.csv", index=False)
# Save and export the DataFrame to a SQLite database named books.db
df.to_sql("books", sqlite3.connect("books.db"), if_exists="replace", index=False)
```

&nbsp; 4. `web_scraping.py` 파일을 저장하고 실행한다. 이전과 같이 데이터 리스트와 길이를 보여주는 출력을 확인한다. 저장하고 내보낸 데이터를 포함하는 파일이나 데이터베이스도 확인해야 한다. 예를 들어 저장하고 내보낸 데이터를 포함하는 `books.csv`, `books.json` 또는 `books.db` 파일이나 `books.db` 데이터베이스의 `books` 테이블을 확인해야 한다.

방금 Python 표준 라이브러리와 pandas 라이브러리를 사용하여 스크랩된 데이터를 파일 또는 데이터베이스에 저장하고 내보냈다. 이러한 라이브러리의 함수와 메서드를 사용하여 다른 형식과 대상에 데이터를 저장하고 내보내는 방법을 보였다. 

## <a name="summary"></a> 요약
Beaytiful Soup와 requests 라이브러리를 사용한 Python 웹 스크래핑에 관한 이 포스팅의 끝에 도달했다. 다음의 방법을 학습하였다.

- Beautiful Soup와 requests를 설치하고 임포트한다.
- 웹 페이지를 가져와 콘텐츠를 출력하는 간단한 웹 스크래핑 스크립트를 만들었다.
- Beautiful Soup로 HTML을 해석하고 웹 페이지의 다양한 요소를 액세스한다.
- 웹 페이지에서 데이터를 추출하여 구조화된 형식으로 저장한다.
- 웹 스크래핑 중 발생할 수 있는 오류와 예외를 처리한다.
- 스크랩된 데이터를 저장하고 파일 또는 데이터베이스로 내보낸다.

이 포스팅을 따르면 웹 스크래핑 프로젝트를 수행하는 데 도움이 될 수 있는 기술과 지식을 얻을 수 있다. 웹 스크래핑은 웹상의 다양한 소스에서 데이터를 수집하고 분석할 수 있는 강력하고 유용한 기술이다. 그러나 웹 스크래핑에는 주의하고 존중해야 할 윤리적이고 법적인 문제가 수반된다. 웹 페이지를 스크래핑하기 전에 항상 약관, `robots.txt` 파일과 스크래핑하려는 웹 사이트의 개인 정보 보호 정책을 확인해야 한다. 또한 너무 자주 또는 너무 공격적으로 스크래핑하는 것을 피해야 하며, 이는 웹 사이트 또는 사용자에게 해를 끼치거나 불편을 줄 수 있다. 또한 스크래핑한 데이터의 소스에 적절한 크레딧과 속성을 부여하고 합법적이고 악의적이지 않은 목적으로 데이ㅇ터를 사용해야 한다.
